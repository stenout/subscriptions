# Полезные подписки

## YouTube

[Sergey Nemchinskiy](https://www.youtube.com/user/pro100fox2/)

Канал в основном про Java, но есть и видео на общепрограммистские темы. В плейлистах есть курсы лекций про архитектуру, базы данных, шаблоны проектирования и т.д.

[Хекслет](https://www.youtube.com/user/HexletUniversity/)

[Kirill Mokevnin](https://www.youtube.com/user/mokevnin/)

Канал курсов Хекслет с вебинарами и стримами на разные темы и личный канал одного из основателей.

[yegor256](https://www.youtube.com/user/technoparkcorp/)

Чувак любит разводить холивары и высказывать спорные мнения, но бывает интересно. Большинство видео на английском, в плейлисте с конференциями много выступлений на русском.

[Dmitry Afanasyev](https://www.youtube.com/user/simpletrainingcom)

Видеоуроки по Laravel

## Telegram

[@addmeto](https://t.me/addmeto) - IT-новости

[@hexlet_ru](https://t.me/hexlet_ru) - тот же Хекслет. Между анонсами видео с YouTube попадаются интересные статьи

## Подкасты

[Pадио-Т](https://radio-t.com/) - Гаджеты, облака, программирование - вот это все

[Цинковый прод](https://soundcloud.com/znprod) - Три PHP тимлида разговаривают о всяком и топят за Rust

[Битовая каска](https://bitcask.live/) - Бывшие ведущие "Разбора полетов" обсуждают с гостями околоайтишные темы.
